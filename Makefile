CROSS_COMPILE=
CC=${CROSS_COMPILE}gcc
TARGET=hello_world
all: ${OBJECTS}
	$(CC) -o ${TARGET} chall.c


clean: 
	rm -f *.o
	rm -f ${TARGET}
